import java.util.Scanner;
class task1 {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);

    System.out.println("Введите высоту и ширину:");

    int h = in.nextInt();
    int w = in.nextInt();
    // Для корректного отображения креста из диагоналей, при пропорциональных высоте и ширине
    long step = (w > h) ? Math.round( w/h ) : Math.round( h/w );

    for (int i = 0 ; i < h; i++){
      for (int j = 0; j < w; j++){
        if ( (j == i * step ) || (j == w - i * step - 1)) {
          System.out.print("1");
        } else {
          System.out.print("0");
        }
      }
      System.out.println();
    }
  }
}
