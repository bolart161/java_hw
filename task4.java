class NuclearBoat {
  private NuclearBoatEngine nbe;

  NuclearBoat(){
    nbe = new NuclearBoatEngine();
  }

  void start(){
    //запуск двигателя...
    nbe.start();
  }


  private class NuclearBoatEngine {
    void start(){
      System.out.println("Boat was started...");
    }
  }
}

public class task4 {
  public static void main(String[] args){
      NuclearBoat nb = new NuclearBoat();
      nb.start();
    }
}
