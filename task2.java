import java.util.Scanner;
import java.util.ArrayList;

class Record {
    String text;
    String autor;
}

class NoteBook {

  public void addRecord(Record newRecord) {
    list.add(newRecord);
  }

  public Record getRecord(int id) {
    return list.get(id);
  }

  public void setRecord(int id, Record newRecord) {
    list.set(id, newRecord);
  }

  public void removeRecord(int id) {
    list.remove(id);
  }

  public void viewList() {
    for ( int i = 0 ; i < list.size(); i++) {
      Record tmpRec = list.get(i);
      System.out.println(tmpRec.autor + ": " + tmpRec.text);
    }
  }

  private ArrayList<Record> list = new ArrayList<Record>();
}

class task2 {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    NoteBook nb = new NoteBook();

    for (int i = 0; i < 3; i++) {
      Record rec = new Record();
      System.out.print("Autor: ");
      rec.autor = in.nextLine();
      System.out.print("Record: ");
      rec.text = in.nextLine();

      nb.addRecord(rec);
    }

    nb.viewList();
  }
}
