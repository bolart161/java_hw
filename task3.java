import java.util.ArrayList;
import java.util.Arrays;

class Record{
  String autor;
  String text;

  Record(String newAutor, String newText){
    autor = newAutor;
    text = newText;
  }

  public void print(){
    System.out.println("Autor: " + autor);
    System.out.println("Text record: " + text);
  }
}

class task3{
   static <T> ArrayList<T> convertArray(T[] arr){
     return new ArrayList<T>(Arrays.asList(arr));
   }


  public static void main(String[] args){
    Record[] recs = { new Record("Artem", "Hello World!"), new Record("Alex", "Hello World!"), new Record("Michel", "Hello World!")};

    for (int i =0; i < recs.length; i++){
      recs[i].print();
    }

    ArrayList<Record> al = convertArray(recs);

    for (int i = 0; i < al.size(); i++){
      Record r = al.get(i);
      r.print();
    }

  }
}
